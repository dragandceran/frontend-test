import React from 'react';
import './app.css';
import Page from './components/Page/Page'

function App() {
  return (
    <div className="app">
     <Page />
    </div>
  );
}

export default App;
