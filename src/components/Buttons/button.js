import React from 'react';
import './button.css'


const Button = (props) => {

    const buttonClass = props.isHidden ? 'button hidden' : 'button'
    return (
        
        <div className={buttonClass}>
            <button onClick={props.onButtonClicked}>
                {props.label}
            </button>
        </div>
 )
}

export default Button