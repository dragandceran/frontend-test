import React from 'react';
import './searchBar.css'


const SearchBar = (props) => { 
    
    const getInputValue = (e) => {
        props.onInputChange(e.target.value)
       
    }
    return (
        <div className="searchbar">
           <form>
               <input className="inputfield" onChange={getInputValue} type="text" placeholder="Search"/>
            </form>
        </div>
 )
}

export default SearchBar