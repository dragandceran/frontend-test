import React from 'react';
import './pokemonImage.css'


const PokemonImage = (props) => {
 const { pokemonImage } = props
    return (
        <div className="pokemonimage">
         <img src={pokemonImage} alt=""></img>
        </div>
 )
}

export default PokemonImage