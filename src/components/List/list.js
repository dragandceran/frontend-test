import React from 'react';
import './list.css'
import axios from 'axios'
import ListItem from '../ListItem/listItem'
import Button from '../Buttons/button'


class List extends React.Component{
    constructor(props){
        super(props);
        this.handleItemClick = this.handleItemClick.bind(this)        
        this.handleNextClick = this.handleNextClick.bind(this)
        this.handlePrevClick = this.handlePrevClick.bind(this)
        this.state = {
            data:[],
            count: 0,
            offset: 0,
            limit: 17,
            nextBtn: false,
            prevBtn: false,
        }
    }
    handleNextClick(e) {
        let offset = this.state.offset + this.state.limit
         this.getData(offset)
      }
    handlePrevClick(e){
        let offset = this.state.offset - this.state.limit
        this.getData(offset)
    }
     handleItemClick(item) {
        this.props.pokemonUrl(item)
     }

    getData(offset=this.state.offset){
        const limit = this.state.limit
        let pokemonList = `https://pokeapi.co/api/v2/pokemon/?offset=${offset}&limit=${limit}`
        axios.get(pokemonList)
        .then(response => {  
            let isNextHidden = (response.data.count - limit) <= offset ? true : false
            let isPrevHidden = offset === 0 ? true : false 
          this.setState({
            count: response.data.count,
            data: response.data.results,
            offset,
            nextBtn: isNextHidden,
            prevBtn: isPrevHidden,
            })
        })
        .catch(error => {
          console.log(error);
        });

    }
    
    componentDidMount() {
     this.getData()
      }


    render(){
    return (
        <div className="list">
           <div className="results">
              {this.state.data.map(item => {
                  return (
                    <div key={item.name}>   
                        <ListItem {...item} onItemClick={this.handleItemClick} />
                    </div>
                     )
                  })
              }
            </div>
            <div  className="buttons">
            <Button onButtonClicked={this.handlePrevClick} label="Prev" isHidden={this.state.prevBtn} />
            <Button onButtonClicked={this.handleNextClick} label="Next" isHidden={this.state.nextBtn} />
            </div>
        </div>
        )
    }
}

export default List