import React from 'react';
import './listItem.css'

const ListItem = (props) => {
  const handleClick = () => {
    props.onItemClick(props.name)
  }
    return (
        <div className="listitem" onClick={handleClick}>
          {props.name}
        </div>
 )
}

export default ListItem