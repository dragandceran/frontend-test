import React from 'react';
import './label.css'

const Label= (props) => {
const name = props.pokemonData.name  
// converting from hectograms to kg
const weight = props.pokemonData.weight / 10
// converting decimeters to cm
const height = props.pokemonData.height * 10
const abilities = props.pokemonData.abilities
    

    return (
        
        <div className="label">
            <div className="info">
                <h3>{name}</h3>
                <p>Weight: {weight} kg</p>
                <p>Height: {height} cm</p>
            </div>

         <div className="abilities">
             <h3>Abilities</h3>
             <div className="abilitylist">
            {abilities.map( (item, i)=> {
                return(
                    <div className="ability" key={`${item.ability.name}_${i}`}>
                       <div> {item.ability.name}</div>
                        </div>
                        )
                })
            } 
                </div>
            </div>
        </div>
 )
}

export default Label