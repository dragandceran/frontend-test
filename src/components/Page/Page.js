import React from 'react'
import './page.css'
import List from '../List/list'
import SearchBar from '../SearchBar/searchBar'
import SearchResults from '../SearchResults/searchResults'
import Warning from '../Warning/warning'
import axios from 'axios'


class Page extends React.Component {
    constructor(props){
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this)
        this.getPokemonUrl = this.getPokemonUrl.bind(this)
        this.state = {
        search: '',
        results:{},
        error:''
        }
    }
    handleInputChange(value) {
        if (value.length >= 3) {
            this.getData(value.toLowerCase())
        }
        
    }
    getPokemonUrl(pokemonName){
       this.getData(pokemonName)
    }

    getData(searchTerm){
        let pokemon = `https://pokeapi.co/api/v2/pokemon/${searchTerm}`
        axios.get(pokemon)
        .then (response =>{
            this.setState({results: response.data})
        })
        .catch(error => {
            this.setState({
                error,
                results: {},
            })
          });
    }


    render(){
        const { results } = this.state
        const isEmpty = Object.keys(results).length === 0
        const { error } = this.state
        console.log(this.state)
    return (
        <div className="wraper">
            <div className="sidebar">
                <h1>Pokédex</h1>
                <h3>List of Pokémon</h3>
                <List pokemonUrl={this.getPokemonUrl} />
            </div>
            <div className="card">
                <SearchBar onInputChange={this.handleInputChange}/>
                {!isEmpty &&  <SearchResults pokemonDetails={results}/>}
                {error && isEmpty && <Warning/>}
            </div>
        </div>
 )
}
}

export default Page