import React from 'react';
import './searchResults.css'
import PokemonImage from '../PokemonImage/pokemonImage'
import Label from '../Label/label'


const SearchResults = (props) => {

    const { front_default } = props.pokemonDetails.sprites
    const pokemonData = props.pokemonDetails

    return ( 
        
        <div className="details">
         <PokemonImage pokemonImage={front_default}/>
         <Label pokemonData={pokemonData}/>
        </div>
 )
}

export default SearchResults