Single page API Test
===
Requirements
---

There should be;
- A working paginated list of pokemon on the left hand side of the page, as shown in the design and click through to browse all possible pokemon.
- Previous pagination button is not shown on the first page of pokemon and next page is not shown on the last
- A search box, which can be used to search for any valid pokemon that exists in the API. This should at least be complete word match i.e `Venusaur`, but can be improved using fuzzy/partial word search i.e. `saur` to list under the search box `Bulbasaur`, `Ivysaur`, `Venusaur` and `Venusaur-mega`.
- If no pokemon are found from the search then this should be clearly displayed to the user. 
- All the results of the search should be displayed below the search box. If more than one pokemon are returned in the results then these should all be listed in the style shown in the design for a Pokémon with their image and abilities.
- The image of the Pokémon should be as shown in the design.
- Update the readme to show the installation method, running a local development copy and compiling production. 
- Match the layout and styling included in the design folder.
- The page is responsive when viewed on Mobile and Desktop

Achived
- A working paginated list of pokemon on the left hand side of the page, as shown in the design and click through to browse all possible pokemon.
- Previous pagination button is not shown on the first page of pokemon and next page is not shown on the last
- A search box, which can be used to search for any valid pokemon that exists in the API. This should at least be complete word match i.e `Venusaur`.
- If no pokemon are found from the search then this should be clearly displayed to the user. 
 All the results of the search should be displayed below the search box. If more than one pokemon are returned in the results then these should all be listed in the style shown in the design for a Pokémon with their image and abilities.
- The image of the Pokémon is shown as in the design.
- Match the layout and styling included in the design folder.
- The page is responsive when viewed on Mobile and Desktop (larger than 1024px and smaller than 500px)

Installation
---

1. Clone repo
2. Run `npm install` to install dependencies.
3. `npm start` for development
4. `npm run build` for production


Build

Webpack is in charge of build. When webpack processes application, it internally builds a dependency graph which maps every module project needs and generates one or more bundles.Since version 4.0.0, webpack does not require a configuration file to bundle project, stil its very configurable.  

